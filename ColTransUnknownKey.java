import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ColTransUnknownKey {
    public static void main(String[] args) {
        ColTransUnknownKey r = new ColTransUnknownKey();
        decryptUnknownKey(r.getFile(args[0]));
    }
    
    // function to decrypt the text without key
    public static void decryptUnknownKey(String msg) {
        char[] myArray = msg.toCharArray(); // convert string to array of char
        char[][] table; // create a table to store the array
        int length = msg.length();
        ArrayList<Integer> keys = getFactors(length);
        
        // loop for every possible keys
        for (int i = 0; i < keys.size(); i++) {
            int myArrayIndex = 0;
            int key = keys.get(i); // get the key
            int rowNum = length / key; // calculate the number of rows
            table = new char[rowNum][key];
            System.out.println("Key: " + key + " ----- " + "Row: " + rowNum);
            
            // use a loop to fill the characters into the table
            for (int col = 0; col < key; col++) {
                for (int row = 0; row < rowNum; row++) {
                    table[row][col] = myArray[myArrayIndex];
                    myArrayIndex++;
                }
            }

            // use a loop to print out the decrypted text
            for (char[] plain : table) {
                for (int j = 0; j < table[0].length; j++) {
                    System.out.print(plain[j]);
                }
            }
            System.out.println("");
        }
    }
    
    // function to get factors
    public static ArrayList<Integer> getFactors(int lengthSize) {
        ArrayList<Integer> keys = new ArrayList<>();
        for (int i = 2; i < lengthSize; i++) {
            if (lengthSize % i == 0) {
                keys.add(i);
            }
        }
        return keys;
    }
    
    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
