import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CeasarThenColTrans {
    public static void main(String[] args) {
        CeasarThenColTrans r = new CeasarThenColTrans();
        decryptUnknownKeyCeasar(r.getFile(args[0]));
    }
    
    // function to decrypt the text without key
    public static void decryptUnknownKeyCeasar(String msg) {
        char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ',', ':', ';', '(', ')', '-', '!', '?', '$', '\'', '\"', '\n',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // the alphabet given by the lecturer
        
        char[] myArray = msg.toCharArray();
        ArrayList<String> stringArray = new ArrayList<>();

        for (int k = 0; k < alphabet.length; k++) {
            String result = "";
            for (int i = 0; i < myArray.length; i++) {
                for (int j = 49; j >= 0; j--) {
                    if (myArray[i] == alphabet[j]) {
                        if ((j - k) < 0) {
                            result += alphabet[50 + (j - k)];
                        } else {
                            result += alphabet[j - k];
                        }
                    }
                }
            }
            stringArray.add(result);
        }
        
        // decrypt with ceasar first then fast each message with key to decrypt with coltrans
        for (int i = 1; i < stringArray.size(); i++) {
            decryptColTrans(i, stringArray.get(i));
        }
    }
    
    public static void decryptColTrans(int key, String msg){
        char[] myArray = msg.toCharArray(); // convert string to array of char
        int myArrayIndex = 0;
        int rowNum = msg.length() / key;
        char[][] table = new char[rowNum][key]; // create a table to store the array
        System.out.println("Key: " + key + " ----- " + "Row: " + rowNum);
        
        // use a loop to fill the characters into the table
        for (int col = 0; col < key; col++) {
            for (int row = 0; row < rowNum; row++) {
                table[row][col] = myArray[myArrayIndex];
                myArrayIndex++;
            }
        }
        
        // use a loop to print out the decrypted text
        for (char[] plain : table) {
            for (int i = 0; i < table[0].length; i++) {
                System.out.print(plain[i]);
            }
        }
        System.out.println("");
    }
    
    // function to get factors
    public static ArrayList<Integer> getFactors(int lengthSize) {
        ArrayList<Integer> keys = new ArrayList<>();
        for (int i = 2; i < lengthSize; i++) {
            if (lengthSize % i == 0) {
                keys.add(i);
            }
        }
        return keys;
    }
    
    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
