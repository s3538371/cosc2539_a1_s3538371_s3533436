import java.util.ArrayList;
import java.io.*;

public class CeasarUnknownKey {
    public static void main(String[] args) {
        CeasarUnknownKey r = new CeasarUnknownKey();
        decryptUnknownKey(r.getFile(args[0]));
    }

    public static void decryptUnknownKey(String msg) {
        char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ',', ':', ';', '(', ')', '-', '!', '?', '$', '\'', '\"', '\n',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // the alphabet given by the lecturer
        
        char[] myArray = msg.toCharArray();
        ArrayList<String> stringArray = new ArrayList<>();
        
        // try to decrypt with every keys
        for (int k = 0; k < alphabet.length; k++) {
            String result = "";
            for (int i = 0; i < myArray.length; i++) {
                for (int j = 49; j >= 0; j--) {
                    if (myArray[i] == alphabet[j]) {
                        if ((j - k) < 0) {
                            result += alphabet[50 + (j - k)];
                        } else {
                            result += alphabet[j - k];
                        }
                    }
                }
            }
            stringArray.add(result);
        }
        
        // print out the possible results
        for (int i = 0; i < stringArray.size(); i++) {
            System.out.println("Key: " + i);
            System.out.println(stringArray.get(i));
        }
    }
    
    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
