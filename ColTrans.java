import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ColTrans {
    public static void main(String[] args) {
        ColTrans r = new ColTrans();

        if ("e".equals(args[0])) {
            int key = Integer.parseInt(args[2]);
            encrypt(key, r.getFile(args[1]));
        }
        else if ("d".equals(args[0])) {
            int key = Integer.parseInt(args[2]);
            decrypt(key, r.getFile(args[1]));
        }
    }
    
    // function to decrypt the text
    public static void decrypt(int key, String msg){
        char[] myArray = msg.toCharArray(); // convert string to array of char
        int myArrayIndex = 0;
        int rowNum = msg.length() / key; // calculate the number of row
        char[][] table = new char[rowNum][key]; // create a table to store the array
        System.out.println("Key: " + key + " ----- " + "Row: " + rowNum);
        
        // use a loop to fill the characters into the table
        for (int col = 0; col < key; col++) {
            for (int row = 0; row < rowNum; row++) {
                table[row][col] = myArray[myArrayIndex];
                myArrayIndex++;
            }
        }
        
        // use a loop to print out the decrypted text
        for (char[] plain : table) {
            for (int i = 0; i < table[0].length; i++) {
                System.out.print(plain[i]);
            }
        }
        System.out.println("");
    }
    
    // function to encrypt the text
    public static void encrypt(int key, String msg) {
        // check if the length is long enough
        if ((msg.length() % key) != 0) {
            int missingCharNum = key - (msg.length() % key);
            
            // fill the missing characters with spaces
            for (int i = 0; i < missingCharNum; i++) {
                msg += " ";
            }
        }
        
        char[] myArray = msg.toCharArray();
        int myArrayIndex = 0;
        int rowNum = msg.length() / key; // calculate the number of row
        char[][] table = new char[rowNum][key]; // create a table to store the array
 
        // use a loop to fill the characters into the table
        for (int row = 0; row < rowNum; row++) {
            for (int col = 0; col < key; col++) {
                table[row][col] = myArray[myArrayIndex];
                myArrayIndex++;
            }
        }
        
        // use a loop to print out the decrypted text
        for (int col = 0; col < key; col++) {
            for (int row = 0; row < rowNum; row++) {
                System.out.print(table[row][col]);
            }
        }
        System.out.println("");
    }
    
    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
