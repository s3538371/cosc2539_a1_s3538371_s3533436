import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SubstCount {
    public static void main(String[] args) {
        SubstCount r = new SubstCount();
        countFrequency(r.getFile(args[0]));
    }
    
    public static void countFrequency(String msg) {
        char[] letter = msg.toCharArray();
        System.out.println(letter);
        Map<Character, Integer> countFrequecyMap = new HashMap<>();
        System.out.println(countFrequecyMap.size());
        // count the times each character appear in the text
        for (char c : letter) {
            if (!countFrequecyMap.containsKey(c))  {
                countFrequecyMap.put(c, 1);
            }  
            else {            
                countFrequecyMap.put(c, countFrequecyMap.get(c) + 1);
            }
        }
        countFrequecyMap.forEach((k,v)->System.out.println("Char : " + k + " Times : " + v));    
    }
    
    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
