import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Vernam {
    public static void main(String[] args) {
        Vernam r = new Vernam();
        
        if ("e".equals(args[0])) {
            int n = Integer.parseInt(args[3]);
            encrypt(r.getFile(args[1]), r.getFile(args[2]), n);
        }
        else if ("d".equals(args[0])) {
            int n = Integer.parseInt(args[3]);
            decrypt(r.getFile(args[1]), r.getFile(args[2]), n);
        }
        else if ("g".equals(args[0])) {
            int n = Integer.parseInt(args[2]);
            generatedKey(args[1], n);
        }
    }

    public static void encrypt(String msg, String key, int offset) {
        char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ',', ':', ';', '(', ')', '-', '!', '?', '$', '\'', '\"', '\n',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // the alphabet given by the lecturer
        
        Vernam r = new Vernam();
        StringBuilder strKey = new StringBuilder(r.checkKey(msg, key));
        strKey = new StringBuilder(r.checkOffset(strKey.toString(), offset));
        
        char[] letterKeyFinal = strKey.toString().toCharArray();
        char[] letter = msg.toCharArray();
        ArrayList<Integer> p = new ArrayList<>();
        ArrayList<Integer> k = new ArrayList<>();
        
        // check and match the index of the key and the msg
        for (char c : letter) {
            second:
            for (int i = 0; i < alphabet.length; i++) {
                if (c == alphabet[i]) {
                    p.add(i);
                    break second;
                }
            }
        }
        for (char c : letterKeyFinal) {
            second:
            for (int i = 0; i < alphabet.length; i++) {
                if (c == alphabet[i]) {
                    k.add(i);
                    break second;
                }
            }
        }
        
        // do the encrypting process
        int[] c = new int[k.size()];
        for (int i = 0; i < c.length; ++i) {
            c[i] = p.get(i) + k.get(i);
        }
        for (int i = 0; i < c.length; i++) {
            if (c[i] > 50) {
                c[i] = c[i] - 49;
            }
        }

        for (int i : c) {
            System.out.print(alphabet[i]);
        }
    }

    public static void decrypt(String msg, String key, int offset) {

        char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ',', ':', ';', '(', ')', '-', '!', '?', '$', '\'', '\"', '\n',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // the alphabet given by the lecturer
        
        Vernam r = new Vernam();
        StringBuilder strKey = new StringBuilder(r.checkKey(msg, key));
        strKey = new StringBuilder(r.checkOffset(strKey.toString(), offset));
        
        char[] letterKeyFinal = strKey.toString().toCharArray();
        char[] letter = msg.toCharArray();
        ArrayList<Integer> keyIndex = new ArrayList<>();
        ArrayList<Integer> msgIndex = new ArrayList<>();
        
        // check and match the index of the key and the msg
        for (char k : letterKeyFinal) {
            for (int j = 0; j < alphabet.length; j++) {
                if (k == alphabet[j]) {
                    keyIndex.add(j);
                }
            }
        }
        for (char m : letter) {
            for (int i = 0; i < alphabet.length; i++) {
                if (m == alphabet[i]) {
                    msgIndex.add(i);
                }
            }
        }
        
        // do the decrypting process
        int[] c = new int[msgIndex.size()];
        for (int i = 0; i < c.length; ++i) {
            if ((msgIndex.get(i) - keyIndex.get(i)) < 0) {
                c[i] = (msgIndex.get(i) - keyIndex.get(i)) + 49;
            } else {
                c[i] = msgIndex.get(i) - keyIndex.get(i);
            }
        }

        for (int i : c) {
            System.out.print(alphabet[i]);
        }
    }
    
    public static void generatedKey(String fileName, int length) {
        char[] alphabet = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '.', ',', ':', ';', '(', ')', '-', '!', '?', '$', '\'', '\"', '\n',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; // the alphabet given by the lecturer
        
        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 50; i++){
            numbers.add(i);
        }
        Collections.shuffle(numbers); // shuffle the alphabet index
        String key = "";
        int value;
        for(int i = 0; i < length; i++){ // get the key depend on the length required
            value = numbers.get(i);
            key += alphabet[value];
        }
     
        try {
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.append(">");
            writer.print(key);
            writer.append("<");
            writer.close();
        } 
        catch (Exception e) {
            // do something
        }
    }
    
    // function for check the offset and then reorder the key
    public String checkOffset(String key, int n) {
        char[] letters = key.toCharArray();
        StringBuilder strKey = new StringBuilder(key);
        
        for (int i = 0; i < n; i++){
           strKey.append(letters[i]);
        }
        for (int i = 0; i < n; i++){
           strKey.deleteCharAt(i);
        }
        
        return strKey.toString();
    }
    
    // check the key
    public String checkKey(String msg, String key) {
        char[] lettersKey = key.toCharArray();
        StringBuilder strBufferKey = new StringBuilder(key);

        int lengthKey = key.length();
        int lengthMsg = msg.length();
        int fixLenght;

        if (lengthKey > lengthMsg) { // if the key longer than the msg, then we will delete the end part of the key
            for (int i = lengthKey - 1; i > lengthMsg - 1; i--) {
                strBufferKey.deleteCharAt(i);
            }
        } else if (lengthKey < lengthMsg) { // if the msg longer than the key, then we will repeat the key
            fixLenght = lengthMsg - lengthKey;
            for (int j = 0; j < fixLenght; j++) {
                strBufferKey.append(lettersKey[j]);
            }
        }

        return strBufferKey.toString();
    }

    // read the input file
    public String getFile(String fileName) {
	String result = "";
        try {
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            fileReader.close();
            
            // delete the > < characters
            stringBuffer.deleteCharAt(0);
            stringBuffer.deleteCharAt(stringBuffer.length() - 2);
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            result += stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
	return result;
    }
}
